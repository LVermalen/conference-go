from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Conference, Location
from common.json import ModelEncoder
import json
from .models import State
from .acls import get_photo, get_weather


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEnocder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "picture_url",
        "room_count",
        "created",
        "updated",

    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation }

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"})
        conference = Conference.objects.create(**content)
        return JsonResponse(conference, encoder=ConferenceDetailEnocder,safe=False)



@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, pk):
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        weather = get_weather(conference.location.city, conference.location.state)
        return JsonResponse({'conference': conference, 'weather': weather}, encoder=ConferenceDetailEnocder, safe=False)
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location"})
        Conference.objects.filter(id=pk).update(**content)
        conference = Conference.objects.get(id=pk)

        return JsonResponse(conference, encoder=ConferenceDetailEnocder, safe=False)



@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == 'GET':
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder, safe=False)
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid state abreviation"}, status=400)
        picture_url_dict = get_photo(content["city"], content["state"])
        content.update(picture_url_dict)
        location = Location.objects.create(**content)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"})
        Location.objects.filter(id=pk).update(**content)
        location = Location.objects.get(id=pk)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
