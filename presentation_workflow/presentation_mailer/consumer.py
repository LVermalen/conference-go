import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_message_approval(ch, method, properties, body):
    print("  Received %r" % body)
    body = json.loads(body)
    name = body["presenter_name"]
    email = body["presenter_email"]
    title = body["title"]
    send_mail(
        'Your presentation has been accepted',
        f"{name}, we're happy to tell you that your presentation {title} has been accepted"'admin@conference.go',
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def process_message_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    body = json.loads(body)
    name = body["presenter_name"]
    email = body["presenter_email"]
    title = body["title"]
    send_mail(
        'Your presentation has been rejected',
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )



while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        queue_dict = {
            "presentation approvals": process_message_approval,
            "presentation rejections": process_message_rejection
        }
        for queue_name, func in queue_dict.items():
            channel.queue_declare(queue=queue_name)
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=func,
                auto_ack=True,
            )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
